import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './general/home/home.component';
import { NavbarComponent } from './general/shared/navbar/navbar.component';
import { FooterComponent } from './general/shared/footer/footer.component';
import { ProductoComponent } from './general/producto/producto.component';
import { EditProductoComponent } from './admin/edit-producto/edit-producto.component';
import { ModalsComponent } from './general/shared/modals/modals.component';
import { BusquedaComponent } from './general/busqueda/busqueda.component';
import { AddProductComponent } from './admin/add-product/add-product.component';
import { LeadsComponent } from './admin/leads/leads.component';
import { ListComponent } from './admin/list/list.component';
import { UpdateComponent } from './admin/update/update.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    ProductoComponent,
    NavbarComponent,
    ModalsComponent,
    EditProductoComponent,
    BusquedaComponent,
    AddProductComponent,
    LeadsComponent,
    ListComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
