import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './general/home/home.component';
import { BusquedaComponent } from './general/busqueda/busqueda.component';
import { ProductoComponent } from './general/producto/producto.component';
import { EditProductoComponent } from './admin/edit-producto/edit-producto.component';
import { AddProductComponent } from './admin/add-product/add-product.component';
import { LeadsComponent } from './admin/leads/leads.component';
import { ListComponent } from './admin/list/list.component';
import { UpdateComponent } from './admin/update/update.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', component: HomeComponent},
    { path: 'producto', component: ProductoComponent },
    { path: 'edit-product', component: EditProductoComponent },
    { path: 'busqueda', component: BusquedaComponent },
    { path: 'agregar', component: AddProductComponent },
    { path: 'leads', component: LeadsComponent },
    { path: 'lista', component: ListComponent },
    { path: 'actualizar', component: UpdateComponent },
    { path: 'actualizar/actualizar/:idPerson', component: UpdateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
