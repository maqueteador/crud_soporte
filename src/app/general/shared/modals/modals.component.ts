import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../../service/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare const $: any;

@Component({
  selector: 'app-modals',
  templateUrl: './modals.component.html',
  styleUrls: ['./modals.component.css']
})
export class ModalsComponent implements OnInit {

  form: FormGroup;

  constructor(
    public personService: PersonService,
    private router: Router
  ) { }

  ngOnInit(): void {

      this.form = new FormGroup({
        name:  new FormControl('', [
            Validators.required,
            Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') 
        ]),
        email: new FormControl('', [ 
            Validators.required, 
            Validators.email 
        ]),
        phone: new FormControl('', [ 
            Validators.required, 
            Validators.pattern("^[0-9]*$") 
        ]),
        password: new FormControl('', [ 
            Validators.required, 
            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$') 
        ]),
        terminos: new FormControl('', [ 
            Validators.required, 
        ]),
      });

      $(".modal-trigger").click(function(){
        var dataModal = $(this).attr("data-modal");
        $("#" + dataModal).css({"display":"Flex"});
      });
      $(".close-modal, .modal-sandbox").click(function(){
        $(".modal").css({"display":"none"});
      });
  }

  get f(){
    return this.form.controls;
  }

  submit(){
    console.log(this.form.value);
    this.personService.create(this.form.value).subscribe(res => {
         alert('Exito al crear tu usuario!');
         this.router.navigateByUrl('/');
    })
  }

}
export class AppModule { }
