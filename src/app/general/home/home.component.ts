import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $('.banner-carousel').owlCarousel({
        nav: false,
        loop: true,
        dots: true,
        lazyLoad: true,
        items: 1,
        autoplay: true,
        animateIn: "fadeIn",
        animateOut: "fadeOut",
        autoplayTimeout: 7500,
        autoplayHoverPause: false,
    });
  }

}