export interface Person {
    id: number;
    name: string;
    email: string;
    password: string;
    phone: number;
    terminos: string;
}
